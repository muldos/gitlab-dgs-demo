# Sample maven project

## Local build and run instructions 

These instructions, by design are not using spring-boot plugin commands, to produce a demo JAR file ready to execute, or deploy to the Jfrog platform.


* Only the first time

  ```jf mvnc ```

* After each changes

  ```
    export PKG_VERSION=1.2.3
    jf mvn -Drevision=$PKG_VERSION -Dmaven.test.skip=true clean package
  ```

* Run the self-executable jar 

  ```java -jar target/dgs-demo-$PKG_VERSION.jar```


Go to http://localhost:8080/graphiql 